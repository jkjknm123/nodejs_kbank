var request = require('request')
var cheerio = require('cheerio')

login(function (cookie) {
    token(cookie, (token) => {
        main(token,(ck) =>{

            get_balance(ck)
        })
    })

})
function get_balance(cookie= null) {
     var url = "https://ebank.kasikornbankgroup.com/retail/cashmanagement/inquiry/AccountSummary.do?action=list_domain1";
     var data = {
       url:url,
       method:"GET",
       headers:{
           "Cookie":cookie
       }
     }
     request(data,(err,res,body) =>{
        var $ = cheerio.load(body)
        var money = $('td[bgcolor=D1F2CB]').text()
        console.log(money)
     })
}
function main(token,cp) {

    var url = "https://ebank.kasikornbankgroup.com/retail/security/Welcome.do";
    var data = {
        url:url,
        method:"POST",
        form:{
            "txtParam":token
        }
    }
    request(data, (err,res,body) =>{
        var JSESSIONID = res.headers['set-cookie'][1].split('JSESSIONID=')
        JSESSIONID = JSESSIONID[1].split(';')
        JSESSIONID = JSESSIONID[0]
        var BIGipServerRSSO = res.headers['set-cookie'][2].split('BIGipServer~EWeb~ibankgroup_pool')
        BIGipServerRSSO = BIGipServerRSSO[1].split(';')
        BIGipServerRSSO = BIGipServerRSSO[0]
        var TS0195fd9d = res.headers['set-cookie'][3].split('TS0195fd9d=')
        TS0195fd9d = TS0195fd9d[1].split(';')
        TS0195fd9d = TS0195fd9d[0]
        var cookie = "JSESSIONID=" + JSESSIONID + ";" + "BIGipServer~EWeb~ibankgroup_pool=" + BIGipServerRSSO + ";TS0195fd9d=" + TS0195fd9d
        cp(cookie)
        
    })
    
}
function login(cp) {

    var url = "https://online.kasikornbankgroup.com/K-Online/login.do"
    var data = {
        url: url,
        method: "POST",
        form: {
            "app": "0",
            "cmd": "authenticate",
            "custType": "",
            "locale": "th",
            "password": "",
            "tokenId": "55555",
            "userName": ''
        }

    }
    request(data, function (err, res, body) {
        var JSESSIONID = res.headers['set-cookie'][1].split('JSESSIONID=')
        JSESSIONID = JSESSIONID[1].split(';')
        JSESSIONID = JSESSIONID[0]
        var tok = res.headers['set-cookie'][2].split('tok=')
        tok = tok[1]
        var BIGipServerRSSO = res.headers['set-cookie'][3].split('BIGipServerRSSO=')
        BIGipServerRSSO = BIGipServerRSSO[1].split(';')
        BIGipServerRSSO = BIGipServerRSSO[0]
        var TS01001929 = res.headers['set-cookie'][4].split('TS01001929=')
        TS01001929 = TS01001929[1].split(';')
        TS01001929 = TS01001929[0]

        var cookie = "JSESSIONID=" + JSESSIONID + ";" + "tok=" + tok + ";" + "BIGipServerRSSO=" + BIGipServerRSSO + ";TS01001929=" + TS01001929
        cp(cookie)


    })

}
function token(cookie = null, cp) {
    url = "https://online.kasikornbankgroup.com/K-Online/ib/redirectToIB.jsp?r=1381"
    var data = {
        url: url,
        method: "GET",
        headers: {
            "Cookie": cookie
        }

    }
    request(data, (err, res, body) => {
        var $ = cheerio.load(body)
        var token = $('input[name=txtParam]').eq(0).val()
        //console.log(token)
        cp(token)

    })


}